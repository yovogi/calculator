// check if given character is a digit
export const isDigit = (character: string): boolean => {
  return /\d/.test(character);
};

// check if given character is one of + - / * ^
export const isMathOperation = (character: string): boolean => {
  return /[+\-*^/]/.test(character);
};
