/* eslint-disable testing-library/prefer-screen-queries */
import { render, fireEvent } from "@testing-library/react";
import Calculator from "./components/Calculator/Calculator";

describe("Calculator application", () => {
  test("if divide by 0 error message is shown", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "3/0" } });
    fireEvent.click(button);
    expect(getByText("You cannot divide by 0")).toBeInTheDocument();
  });

  test("if invalid expression error message is shown", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "wtf" } });
    fireEvent.click(button);
    expect(
      getByText("Not a valid expression. Please check your input!")
    ).toBeInTheDocument();
  });

  test("if addition is correct", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "12+2243" } });
    fireEvent.click(button);
    expect(getByText("Result: 2255")).toBeInTheDocument();
  });

  test("if substract is correct", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "12-10" } });
    fireEvent.click(button);
    expect(getByText("Result: 2")).toBeInTheDocument();
  });

  test("if factorial is correct", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "10!" } });
    fireEvent.click(button);
    expect(getByText("Result: 3628800")).toBeInTheDocument();
  });

  test("if pow is correct", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "14^8" } });
    fireEvent.click(button);
    expect(getByText("Result: 1475789056")).toBeInTheDocument();
  });

  test("if expressions with brackets are correctly resolved", () => {
    const { getByRole, getByText } = render(<Calculator />);
    const input = getByRole("textbox");
    const button = getByText("Calculate");
    fireEvent.change(input, { target: { value: "(1+2)*3" } });
    fireEvent.click(button);
    expect(getByText("Result: 9")).toBeInTheDocument();
  });
});
