import { InputError, dividebyZeroError, invalidExpressionError } from "./error";

export const getOperationPriority = (operation: string): number => {
  if (operation === "+" || operation === "-") {
    return 1;
  } else if (operation === "*" || operation === "/") {
    return 2;
  } else if (operation === "^") {
    return 3;
  } else {
    return 0;
  }
};

export const executeOperation = (
  operation: string,
  firstNumber: number,
  secondNumber: number,
  setError: (value: React.SetStateAction<InputError | null | undefined>) => void
): number => {
  console.log(
    "Operation executed",
    "first number:" +
      firstNumber +
      " second number:" +
      secondNumber +
      " operation:" +
      operation
  );
  if (operation === "+") {
    return firstNumber + secondNumber;
  } else if (operation === "-") {
    // ! since we are popping elements from array actually secondNumber is the one that sits on the left side of the operation
    return secondNumber - firstNumber;
  } else if (operation === "/") {
    if (firstNumber === 0) {
      setError(dividebyZeroError);
    }
    return secondNumber / firstNumber;
  } else if (operation === "*") {
    return secondNumber * firstNumber;
  } else if (operation === "^") {
    return Math.pow(secondNumber, firstNumber);
  } else {
    setError(invalidExpressionError);
    return 0;
  }
};
