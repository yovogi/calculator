import React, { useState } from "react";
import { InputError, invalidExpressionError } from "../../utils/error";
import { isDigit, isMathOperation } from "../../utils/regexHelpers";
import {
  executeOperation,
  getOperationPriority,
} from "../../utils/helperFunctions";

const Calculator: React.FC = () => {
  const [error, setError] = useState<InputError | null>();
  const [result, setResult] = useState<string>("");
  const [expression, setExpression] = useState<string>("");

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setExpression(e.target.value);
  };

  const calculateExpression = () => {
    // removing empty spaces if any
    let expressionWithoutEmptySpaces = expression.replaceAll(" ", "");

    //clearing previous errors
    setError(null);

    // here we store the numbers and operations
    let numbers: number[] = [];
    let operations: string[] = [];

    // we are iterating through the whole expression and handle 5 different cases - digit, operation, opening bracket, closing bracket and factorial
    for (let i = 0; i < expressionWithoutEmptySpaces.length; i++) {
      const currentCharacter = expressionWithoutEmptySpaces.charAt(i);

      //CASE DIGIT - we only add the number to the array with no operations
      if (isDigit(currentCharacter)) {
        let num = currentCharacter;

        // looking and adding all digits to the number
        while (isDigit(expressionWithoutEmptySpaces.charAt(i + 1))) {
          num += expressionWithoutEmptySpaces.charAt(i + 1);
          i++;
        }
        // once we have all the digits of the number, it is pushed into the array
        numbers.push(+num);

        //CASE OPERATION - executing operations if they have higher priority
      } else if (isMathOperation(currentCharacter)) {
        // check if we already have an operations that can be executed
        let isThereAvailableOperation =
          getOperationPriority(operations[operations.length - 1]) >=
          getOperationPriority(currentCharacter);

        while (operations.length > 0 && isThereAvailableOperation) {
          //Replace 2 numbers and 1 operation with their result
          const firstNumber = numbers.pop()!;
          const secondNumber = numbers.pop()!;
          const operation = operations.pop()!;

          const resultOfOperation = executeOperation(
            operation,
            firstNumber,
            secondNumber,
            setError
          );

          numbers.push(resultOfOperation);

          //updating while condition
          isThereAvailableOperation =
            getOperationPriority(operations[operations.length - 1]) >=
            getOperationPriority(currentCharacter);
        }
        operations.push(currentCharacter);

        // CASE OPENING BRACKET - just adding the operation, nothing to do here
      } else if (currentCharacter === "(") {
        operations.push(currentCharacter);

        // CASE CLOSING BRACKET - execute all operations within the brackets
      } else if (currentCharacter === ")") {
        while (operations[operations.length - 1] !== "(") {
          const firstNumber = numbers.pop()!;
          const secondNumber = numbers.pop()!;
          const operation = operations.pop()!;
          const resultOfOperation = executeOperation(
            operation,
            firstNumber,
            secondNumber,
            setError
          );
          numbers.push(resultOfOperation);
        }
        operations.pop();

        // CASE ! - always with highest priority execute it immidiately
      } else if (currentCharacter === "!") {
        const previousCharacter = expressionWithoutEmptySpaces.charAt(i - 1);

        //Logging execution of factorial
        console.log("Factorial executed with number:", previousCharacter);
        if (!numbers[numbers.length - 1] || !isDigit(previousCharacter)) {
          setError(invalidExpressionError);
        } else {
          let factorialResult = 1;
          for (let i = numbers[numbers.length - 1]; i > 1; i--) {
            factorialResult *= i;
          }

          //Removing the number and adding the calculated factorial on its place
          numbers.pop();
          numbers.push(factorialResult);
        }
      }

      // Logging the iteration cycles for debugging purposes
      console.log(currentCharacter, numbers, operations);
    }

    while (operations.length > 0) {
      const firstNumber = numbers.pop()!;
      const secondNumber = numbers.pop()!;
      const operation = operations.pop()!;
      const resultOfOperation = executeOperation(
        operation,
        firstNumber,
        secondNumber,
        setError
      );
      numbers.push(resultOfOperation);
    }

    try {
      let expressionFinalResult = numbers.pop()!.toString();
      setResult(expressionFinalResult);
      if (expressionFinalResult === "NaN") {
        setError(invalidExpressionError);
      }
    } catch (e: any) {
      setError(invalidExpressionError);
    }
  };

  const inputStyles = {
    padding: "10px",
    fontSize: "16px",
    border: "2px solid gray",
    borderRadius: "5px",
    outline: "none",
    boxShadow: "none",
    width: "300px",
    transition: "border-color 0.3s ease",
  };

  const buttonStyles = {
    padding: "10px",
    fontSize: "16px",
    backgroundColor: "#268dff",
    color: "white",
    border: "none",
    borderRadius: "5px",
    marginLeft: "10px",
    cursor: "pointer",
    transition: "background-color 0.5s ease",
  };
  return (
    <div style={{ width: "500px" }}>
      <input
        style={inputStyles}
        type="text"
        value={expression}
        onChange={handleChange}
      />
      <button
        style={buttonStyles}
        onMouseOver={(e) => {
          e.currentTarget.style.backgroundColor = "#0a0aff";
        }}
        onMouseLeave={(e) => {
          e.currentTarget.style.backgroundColor = "#268dff";
        }}
        onClick={calculateExpression}>
        Calculate
      </button>
      {!error && result && <p>Result: {result}</p>}
      {error && (
        <>
          <p style={{ color: "red" }}>{error.reason}</p>
          <img
            style={{ maxWidth: "500px", maxHeight: "380px" }}
            src={error.image}
            alt="error"
          />
        </>
      )}
    </div>
  );
};

export default Calculator;
