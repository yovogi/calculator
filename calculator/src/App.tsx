import Calculator from "./components/Calculator/Calculator";
import sound from "./time.mp3";

const App: React.FC = () => {
  return (
    <div
      style={{
        height: "100vh",
        backgroundImage:
          "url('https://images.unsplash.com/photo-1509228627152-72ae9ae6848d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80')",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}>
      <audio controls>
        <source src={sound} type="audio/mpeg" />
        Your browser does not support the audio element.
      </audio>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          width: "500px",
          margin: "0 auto",
          padding: "25px",
          border: "solid 10px gray",
          backgroundColor: "rgba(255,255,255, 0.7)",
          borderRadius: "15px",
        }}>
        <h1
          style={{ marginBottom: "50px", transition: "scale 1s ease" }}
          onMouseOver={(e) => {
            e.currentTarget.style.scale = "1.5";
          }}
          onMouseLeave={(e) => {
            e.currentTarget.style.scale = "1";
          }}>
          Calculator
        </h1>

        <Calculator />
      </div>
    </div>
  );
};

export default App;
