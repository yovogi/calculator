export type InputError = {
  reason: string;
  image: string;
};

export const invalidExpressionError: InputError = {
  reason: "Not a valid expression. Please check your input!",
  image:
    "https://i.kym-cdn.com/entries/icons/original/000/002/144/You_Shall_Not_Pass!_0-1_screenshot.jpg",
};

export const dividebyZeroError: InputError = {
  reason: "You cannot divide by 0",
  image:
    "https://1.bp.blogspot.com/_GVA115I1I8Y/TQoO62qW1aI/AAAAAAAABOo/DTgXwIUg3J4/s1600/division+by+zero.jpg",
};
